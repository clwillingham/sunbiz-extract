// import https from "https";
import fetch from "node-fetch";
import { FixedWidthParser } from "fixed-width-parser";
import { join } from 'path'

const parser = new FixedWidthParser([
    {
        type: 'string',
        name: 'id',
        start: 0, width: 12
    },
    {
        type: 'string',
        name: 'name',
        padPosition: "end",
        start: 13, width: 191
    },
    {
        type: 'string',
        name: 'status',
        start: 204, width: 1
    },
    {
        type: 'string',
        name: 'type',
        padPosition: "end",
        start: 205, width: 14
    },
    {
        type: 'string',
        name: 'address_1',
        padPosition: "end",
        start: 220, width: 42
    },
    {
        type: 'string',
        name: 'address_2',
        padPosition: "end",
        start: 262, width: 42
    },
    {
        type: 'string',
        name: 'city',
        padPosition: "end",
        start: 304, width: 28
    },
    {
        type: 'int',
        name: 'zip',
        // padPosition: "end",
        start: 334, width: 10
    },

])

async function login(){
    const res = await fetch('https://sftp.floridados.gov/', {
        method: 'post',
        body: 'request=login&username=Public&password=PubAccess1845!',
        headers: {
            'sec-fetch-mode': 'cors',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-dest': 'empty',
            'origin': 'https://sftp.floridados.gov/',
            'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            cookie: 'Authentication=VanDyke',
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.41 Safari/537.36'
        }
    });
    return res.headers.get('set-cookie').split(';').filter((cookie)=>cookie.indexOf('vandyke-auth-token') !== -1)[0];

}

async function get(filePath, token){
    console.log('Authentication=VanDyke;'+token+';home-directory=/Public; root-directory=/;idle-timeout=-180')
    const res = await fetch(join('https://sftp.floridados.gov/', filePath), {
        method: 'get',
        headers: {
            'sec-fetch-mode': 'navigate',
            'sec-fetch-site': 'same-origin',
            'sec-fetch-dest': 'document',
            'sec-fetch-user': '?1',
            'origin': 'https://sftp.floridados.gov/',
            'content-type': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
            cookie: 'Authentication=VanDyke;'+token,
            'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/101.0.4951.41 Safari/537.36'
        }
    })
    return res.text();
}

(async function(){
    const token = await login();
    console.log(token);
    const file = await get('/Public/doc/cor/20220505c.txt', token);
    // console.log(file);
    console.log(parser.parse(file));
})()

